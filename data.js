const json = [
  {
    title: 'Entertainment',
    name: 'Mall Kelapa Gading',
    city: 'Kelapa Gading, Jakarta Utara',
    tag: ['mall', 'building'],
    points: {
      lat: -6.1571933,
      lng: 106.9078818
    }
  },
  {
    title: 'Bussiness',
    name: 'Menara BTN',
    city: 'Gajah Mada, Jakarta Pusat',
    tag: ['building', 'bank'],
    points: {
      lat: -6.1672071,
      lng: 106.8195732
    }
  },
  {
    title: 'Entertainment',
    name: 'Monumen Nasional',
    city: 'Jakarta Pusat',
    tag: ['building', 'history'],
    points: {
      lat: -6.1751393,
      lng: 106.8269815
    }
  },
  {
    title: 'Nature',
    name: 'Taman Nasional Halimun Salak',
    city: 'Sukabumi',
    tag: ['garden', 'nature'],
    points: {
      lat: -6.6759665,
      lng: 106.5401974
    }
  },
  {
    title: 'Family',
    name: 'Wetan',
    city: 'Bandung',
    tag: ['nature', 'mountain'],
    points: {
      lat: -6.5294689,
      lng: 107.2125305
    }
  },
  {
    title: 'Restaurant',
    name: 'Rumah Padang Aris',
    city: 'Jakarta',
    tag: ['restaurant', 'food'],
    points: {
      lat: -6.3646779,
      lng: 106.5514634
    }
  },
  {
    title: 'House',
    name: 'Bumi Cianjur',
    city: 'Bogor',
    tag: ['house', 'food'],
    points: {
      lat: -6.808162,
      lng: 107.0890646
    }
  },
  {
    title: 'Beach',
    name: 'Pantai Indah Kapuk',
    city: 'Jakarta',
    tag: ['beach', 'nature', 'ocean'],
    points: {
      lat: -6.1022945,
      lng: 106.7390232
    }
  },
  {
    title: 'Beach',
    name: 'Palm Bay Water Park',
    city: 'Jakarta',
    tag: ['beach', 'nature', 'ocean'],
    points: {
      lat: -6.1271606,
      lng: 106.7086661
    }
  },
  {
    title: 'Entertainment',
    name: 'Como Park',
    city: 'Jakarta Selatan',
    tag: ['building', 'business'],
    points: {
      lat: -6.2672543,
      lng: 106.755553
    }
  }
];

module.exports = json;