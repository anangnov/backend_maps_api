const express = require("express");
const debug = require("debug")("server");
const cors = require("cors");
const json = require("./data");

const app = express();
const port = process.env.SERVER_PORT || 3001;

app.use(cors());

app.use(
  cors({
    origin: "https://test-next-genp.vercel.app/",
    methods: "GET"
  })
);

app.get("/api/data-json", (req, res) => {
  res.send({
    message: "Success",
    data: json,
    total: json.length
  });
});

app.listen(port, () => debug(`Listening on port ${port}`));
console.log('App running on port ' + port)
